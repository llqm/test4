SrcFiles=$(wildcard *.c)                   #注释：可以得到所有的.c文件
ObjFiles=$(patsubst %.c,%.o,$(SrcFiles))   #把所有的.c文件替换成.o文件jjhvh
 
app:$(ObjFiles)
    gcc -o app $^
 
all:$(ObjFiles)    #伪目标
 
%.o:%.c               #把每一个.c生成.o      
    gcc -o $@ $^  
 
.RHONY:clean       #防止有歧义
 
clean:
	rm -f all
